lazy-docker-compose
=========

Manage Docker Compose services and their dependencies (directories, permissions etc.) on Docker hosts

This role copies all files recursively that have been specified in `docker_compose_project_src` to the destination `docker_compose_project_dest`.

Requirements
------------

**Collections:**
- community.docker

**The following packages need to be installed on the system:**
  - Docker
  - Docker Compose

Role Variables
--------------
The default values for the variables are set in `defaults/main.yml`:
```yml
allow_bind_dns_port: no # Set this to yes if you want to allow containers to bind the DNS Port 53. This will disable DNSStubListener in systemd-resolv

# Optional: Customize /etc/docker/daemon.json config (YAML dict will be converted to JSON automatically)
docker_daemon_config: {}
#  registry-mirrors:
#    - "https://registry.example.com"

# Optional: Create Docker networks that may be required by Docker Compose services as external networks
create_docker_networks: []
# - traefik
```

Example Inventory
-----------------
```yml
all:
  children:
    docker:
      hosts:
        docker-01:
          ansible_host: 192.168.1.5
```

Example Projects 
----------------
```
inventory/docker-compose
└── test
    ├── adguardhome
    │   ├── conf
    │   │   └── AdGuardHome.yaml
    │   ├── docker-compose.yml
    │   └── work
    ├── portainer
    │   ├── data
    │   └── docker-compose.yml
    └── traefik
        ├── data
        │   ├── config.yml
        │   └── traefik.yml
        └── docker-compose.yml
```

Example Playbook
----------------
```yml
- hosts: docker-01
  become: yes
  gather_facts: yes
  roles:
    - role: lazy-docker-compose
      vars:
        allow_bind_dns_port: no # Set this to yes if you want to allow containers to bind the DNS Port 53. This will disable DNSStubListener in systemd-resolv
        lazy_docker_compose_services:
          # Traefik service
          - docker_compose_project_src: "{{ inventory_dir }}/docker-compose/test/traefik/"
            docker_compose_project_dest: /opt/docker/traefik
            modify_permissions: # Optional: Modify permissions
              - path: /opt/docker/traefik/docker-compose.yml
                mode: "0640"
                owner: user_test
                group: user_test
              - path: /opt/docker/traefik/test
                mode: "0600"
                recurse: yes
            touch_empty_files: # Optional: Dynamic files that are initially empty and need to be created and won't be overwritten with future executions!
              - path: /opt/docker/traefik/data/acme.json
                mode: "0600"
            docker_compose_extra_options: "--remove-orphans" # Optional: Extra parameters and options that will be appended to the docker-compose up -d command
          # AdGuardHome service
          - docker_compose_project_src: "{{ inventory_dir }}/docker-compose/test/adguardhome/"
            docker_compose_project_dest: /opt/docker/adguardhome
          # Portainer service
          - docker_compose_project_src: "{{ inventory_dir }}/docker-compose/test/portainer/"
            docker_compose_project_dest: /opt/docker/portainer          
      tags:
        - lazy-docker-compose
```

License
-------

MIT

Author Information
------------------

Oleg Franko 